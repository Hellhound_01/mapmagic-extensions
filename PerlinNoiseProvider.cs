﻿/**
  @class  PerlinNoiseProvider
  @date   10/03/2015
  @author Hellhound

  @brief 
  Implementation a improved Perlin noise algorithm for 1D, 2D, 3D.
  
  This implementation use a seed for pseudo random value creation of the 
  permutation lookup table. Using the same seed will always create the same 
  noise results. 
  
  @remark This algorithm was originally designed by Ken Perlin, but my code 
  is based on the noise tutorial of catlike coding.
  
  @see http://catlikecoding.com/unity/tutorials/noise/
    	
  @remark A good description of the algorithm itself could be found here.
  @see http://http.developer.nvidia.com/GPUGems2/gpugems2_chapter26.html

  Updated :
  Status  : FINAL
  Copyright Binary Revolution, Inc.  All rights reserved.
*/

using System.Linq;
using UnityEngine;
	
namespace MapMagicExternalGenerators
{
	public class PerlinNoiseProvider : GradientNoiseProvider 
	{			
		private const int hashMask = 255;
		
		private static float[] gradients1D = {
			1f, -1f
		};

		private const int gradientsMask1D = 1;
		
		private static Vector2[] gradients2D = {
			new Vector2( 1f, 0f),
			new Vector2(-1f, 0f),
			new Vector2( 0f, 1f),
			new Vector2( 0f,-1f),
			new Vector2( 1f, 1f).normalized,
			new Vector2(-1f, 1f).normalized,
			new Vector2( 1f,-1f).normalized,
			new Vector2(-1f,-1f).normalized
		};
		
		private const int gradientsMask2D = 7;

		private static Vector3[] gradients3D = {
			new Vector3( 1f, 1f, 0f),
			new Vector3(-1f, 1f, 0f),
			new Vector3( 1f,-1f, 0f),
			new Vector3(-1f,-1f, 0f),
			new Vector3( 1f, 0f, 1f),
			new Vector3(-1f, 0f, 1f),
			new Vector3( 1f, 0f,-1f),
			new Vector3(-1f, 0f,-1f),
			new Vector3( 0f, 1f, 1f),
			new Vector3( 0f,-1f, 1f),
			new Vector3( 0f, 1f,-1f),
			new Vector3( 0f,-1f,-1f),
			
			new Vector3( 1f, 1f, 0f),
			new Vector3(-1f, 1f, 0f),
			new Vector3( 0f,-1f, 1f),
			new Vector3( 0f,-1f,-1f)
		};
		
		private const int gradientsMask3D = 15;
		
		/** 
		@brief Create the Simplex Noise instance based on given seed.
		@note This seed must be > 0 otherwise the actual time stamp of the System.Random 
			  class will be used as default value.       
		@param seed The seed used for random value creation. (Default == 0).
		*/
		public PerlinNoiseProvider(int seed=0)
		:base(seed)
		{}

		/**
		@brief Generate a 1D Perlin noise value.
		@param x  The x coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The Perlin noise value in range [-1,1]. 
		*/
		public override float Generate(float x, float frequency)
		{
			int i0 = Mathf.FloorToInt(x*frequency);
			float t0 = x - i0;
			float t1 = t0 - 1f;
			i0 &= hashMask;
			int i1 = i0 + 1;
			
			float g0 = gradients1D[this.permutation[i0] & gradientsMask1D];
			float g1 = gradients1D[this.permutation[i1] & gradientsMask1D];

			float v0 = g0 * t0;
			float v1 = g1 * t1;

			float t = Smooth(t0);
			return Mathf.Lerp(v0, v1, t) * 2f;
		}

		/**
		@brief Generate a 2D Perlin noise value.
		@param x  The x coordinate.
		@param y  The y coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The Perlin noise value in range [-1,1]. 
		*/
		public override float Generate(float x, float y, float frequency)
		{
			var point = new Vector2(x,y);
			point *= frequency;
			
			int ix0 = Mathf.FloorToInt(point.x);
			int iy0 = Mathf.FloorToInt(point.y);
			
			float tx0 = point.x - ix0;
			float ty0 = point.y - iy0;
	
			float tx1 = tx0 - 1f;
			float ty1 = ty0 - 1f;
			ix0 &= hashMask;
			iy0 &= hashMask;
			int ix1 = ix0 + 1;
			int iy1 = iy0 + 1;
			
			int h0 = this.permutation[ix0];
			int h1 = this.permutation[ix1];
			
			Vector2 g00 = gradients2D[this.permutation[h0 + iy0] & gradientsMask2D];
			Vector2 g10 = gradients2D[this.permutation[h1 + iy0] & gradientsMask2D];
			Vector2 g01 = gradients2D[this.permutation[h0 + iy1] & gradientsMask2D];
			Vector2 g11 = gradients2D[this.permutation[h1 + iy1] & gradientsMask2D];

			float v00 = Dot(g00, tx0, ty0);
			float v10 = Dot(g10, tx1, ty0);
			float v01 = Dot(g01, tx0, ty1);
			float v11 = Dot(g11, tx1, ty1);
			
			float tx = Smooth(tx0);
			float ty = Smooth(ty0);
			
			var noise = Mathf.Lerp( Mathf.Lerp(v00, v10, tx),
									Mathf.Lerp(v01, v11, tx),
									ty);
			
			return noise * Mathf.Sqrt(2f);
		}
			
		/**
		@brief Generate a 3D Perlin noise value.
		@param x  The x coordinate.
		@param y  The y coordinate.
		@param z  The z coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The Perlin noise value in range [-1,1]. 
		*/
		public override float Generate(float x, float y, float z, float frequency)
		{		
			var point = new Vector3(x,y,z);
			point *= frequency;
		
			int ix0 = Mathf.FloorToInt(point.x);
			int iy0 = Mathf.FloorToInt(point.y);
			int iz0 = Mathf.FloorToInt(point.z);
			
			float tx0 = point.x - ix0;
			float ty0 = point.y - iy0;
			float tz0 = point.z - iz0;
			
			float tx1 = tx0 - 1f;
			float ty1 = ty0 - 1f;
			float tz1 = tz0 - 1f;
			
			ix0 &= hashMask;
			iy0 &= hashMask;
			iz0 &= hashMask;
			
			int ix1 = ix0 + 1;
			int iy1 = iy0 + 1;
			int iz1 = iz0 + 1;
			
			int h0 = this.permutation[ix0];
			int h1 = this.permutation[ix1];
			int h00 = this.permutation[h0 + iy0];
			int h10 = this.permutation[h1 + iy0];
			int h01 = this.permutation[h0 + iy1];
			int h11 = this.permutation[h1 + iy1];
			
			Vector3 g000 = gradients3D[this.permutation[h00 + iz0] & gradientsMask3D];
			Vector3 g100 = gradients3D[this.permutation[h10 + iz0] & gradientsMask3D];
			Vector3 g010 = gradients3D[this.permutation[h01 + iz0] & gradientsMask3D];
			Vector3 g110 = gradients3D[this.permutation[h11 + iz0] & gradientsMask3D];
			Vector3 g001 = gradients3D[this.permutation[h00 + iz1] & gradientsMask3D];
			Vector3 g101 = gradients3D[this.permutation[h10 + iz1] & gradientsMask3D];
			Vector3 g011 = gradients3D[this.permutation[h01 + iz1] & gradientsMask3D];
			Vector3 g111 = gradients3D[this.permutation[h11 + iz1] & gradientsMask3D];

			float v000 = Dot(g000, tx0, ty0, tz0);
			float v100 = Dot(g100, tx1, ty0, tz0);
			float v010 = Dot(g010, tx0, ty1, tz0);
			float v110 = Dot(g110, tx1, ty1, tz0);
			float v001 = Dot(g001, tx0, ty0, tz1);
			float v101 = Dot(g101, tx1, ty0, tz1);
			float v011 = Dot(g011, tx0, ty1, tz1);
			float v111 = Dot(g111, tx1, ty1, tz1);

			float tx = Smooth(tx0);
			float ty = Smooth(ty0);
			float tz = Smooth(tz0);
			
			return Mathf.Lerp(
				Mathf.Lerp(Mathf.Lerp(v000, v100, tx), Mathf.Lerp(v010, v110, tx), ty),
				Mathf.Lerp(Mathf.Lerp(v001, v101, tx), Mathf.Lerp(v011, v111, tx), ty),
				tz);			
		}
			
		/**
		@brief Gradient function for 2D values.
					
		@param g The 2D vector containing the gradient values.
		@param x The actual x coordinate.
		@param y The actual y coordinate.
		@return float The dot product of the gradient vector.
		*/
		private float Dot (Vector2 g, float x, float y) {
			return g.x * x + g.y * y;
		}

        /**
        @brief Gradient function for 2D values.
            
        @param g The 2D vector containing the gradient values.
        @param x The actual x coordinate.
        @param y The actual y coordinate.
        @return float The dot product of the gradient vector.
        */
        private float Dot (Vector3 g, float x, float y, float z) {
		    return g.x * x + g.y * y + g.z * z;
		}
			
		/**
		@brief Smooth function to turn linear interpolates noise values to curve.
					
		@param t The linear interpolated noise to smooth.
		@return float The smoothed value.
		*/
		private float Smooth (float t) {
			return t * t * t * (t * (t * 6f - 15f) + 10f);
		}
	}
}