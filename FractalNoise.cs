﻿/**
  @class  FractalNoise
  @date   15/03/2016
  @author Hellhound

  @brief 
  Fractal Brownian Motion (fBm) noise value generator for 1D, 2D, 3D.
    
  Fractional Brownian Motion is the summation of successive octaves of noise, each with 
  higher frequency and lower amplitude.
	
  @see https://code.google.com/p/fractalterraingeneration/wiki/Fractional_Brownian_Motion
  
  @remark
  This generator uses the Improved Perlin noise or Perlin Simplex noise algorithm to generate
  the noise value for each octave. 
  
  @remark
  This generator supports also the usage of the fBm derivative billow and ridge noise 
  algorithm. 
  
  Updated :
  Status  : FINAL
  Copyright Binary Revolution, Inc.  All rights reserved.
*/
using System.Collections.Generic;
using UnityEngine;
using MapMagic;
using System;

namespace MapMagicExternalGenerators
{
	[System.Serializable]
	[GeneratorMenu(menu = "External", name = "FractalNoise", disengageable = true)]
	public class FractalNoise : Generator
	{
		//input and output properties      
		public Input input = new Input("Input", InoutType.Map, mandatory: false);
		public Output output = new Output("Output", InoutType.Map);

		//including in enumerator
		public override IEnumerable<Input> Inputs() { yield return input; }
		public override IEnumerable<Output> Outputs() { yield return output; }

		// set initial seed is set to zero to ensure that only the global seed is used
		public int seed = 0;

		// the global terrain resolution
		private int hres = 513;

		// scale factor  
		public float scale = 1.0f;

		// {@link GradientNoiseProvider} values
		// The number of fraction of noise to sum.
		public int octaves = 8; 

		// Defines the scale multiple factor of frequency between octaves.
		public float lacunarity = 2.0f;

		/* Defines weighting of individual frequencies. 
		   Each step, the signal is multiplied by Persistence. */
		public float persistence = 0.5f;

		// The initial frequency value
		public float frequency = 1.25f;

        // Offset value for ridged noise algorithm
        public float ridgedOffset = 1.0f;

        // reference to noise value generator
        private GradientNoiseProvider noise = null;
	
		public enum Base { Perlin, Simplex};
		public Base module = Base.Perlin;

		public enum Type { Fractal, Billow, Ridged };
		public Type type = Type.Fractal;

		// Delegate declaration for dynamic noise method binding
		private delegate float NoiseDelegate(float x, float y);
		private NoiseDelegate generateNoise = null;
	
		/* List of precomputed spectral weights for each frequency to create ridges */
		private List<float> spectralWeights = new List<float>();
		private int maxOctaves = 30;
		private bool dirtySpectralWeights = true;
		
		/** 
		@brief Create a new Fractal noise instance.
		@remark Set Perlin Noise provider using fBm algorithm as default.
		*/
		public FractalNoise(){}		
		
		/**
		@brief Generator method of the Editor node.
		@param tile The MapMagic specific actual terrain chunk/tile object. 
		*/
		public override void Generate(MapMagic.Chunk tile, Biome biome = null)
		{		
			if (tile.stop) return;

            // Use input values if given, otherwise use default
            Matrix src = (Matrix)input.GetObject(tile);
			if (src == null) src = tile.defaultMatrix;

			// create destination matrix
			Matrix dst = new Matrix(src.rect);

			// determine the tile border value
			Coord cmin = src.rect.Min;
			Coord cmax = src.rect.Max;
							
			// determine actual global terrain resolution
			this.hres = MapMagic.MapMagic.instance.resolution;

            /* Map Magic is using multi-threading. By this reason it's better never 
			   ever change generator parameters from generate function or use lock
			   when doing it. */
            lock (spectralWeights)
			{
                if (this.noise == null) this.initNoiseProvider(this.module, this.seed + MapMagic.MapMagic.instance.seed);
                if (this.generateNoise == null) this.initNoiseMethod(this.type);
                this.ComputeSpectralWeights();

				// set noise results
				for (int x = cmin.x; x < cmax.x; x++) {
					for (int z = cmin.z; z < cmax.z; z++)
                    {
                        var normalizedX = (float)x / (hres- 1);
                        var normalizedZ = (float)z / (hres - 1);
                        
                        var value = this.generateNoise(normalizedX, normalizedZ);
                        dst[x, z] = (value / 2f) + 0.5f;
                        dst[x, z] += src[x, z];
                        dst[x, z] *= this.scale;
                    }
				}
			}

			if (tile.stop) return;
			output.SetObject(tile, dst);
		}

		/**
		@brief NodeEditor method to render the node.
		@param layout The MapMagic specific UILayout object. 
		*/
		public override void OnGUI()
		{
			layout.Par(20); input.DrawIcon(layout); output.DrawIcon(layout);
			layout.Field<Base>(ref this.module, "Base");

			/* check if UI element has changed to force noise provider initialization 
			   update, also check if it has not initialized yet */
			if(layout.lastChange || this.noise == null){
				this.initNoiseProvider(this.module, this.seed + MapMagic.MapMagic.instance.seed);
			}		
				
			layout.Field<Type>(ref this.type, "Algorithm");
		
			/* check if UI element has changed to force noise delegate initialization 
			   update, also check if it has not initialized yet */
			if(layout.lastChange || this.generateNoise == null){
				this.initNoiseMethod(this.type);
			}

			// generic settings
			layout.Par(20);
			layout.Field(ref this.seed, "Seed", min: 0);
		
			/* check if UI element has changed to force reinitialize of the permutation
			   table of the noise provider to regard seed changes */
			if(layout.lastChange){
				this.noise.SetSeed(this.seed + MapMagic.MapMagic.instance.seed);
			}
				
			layout.Field(ref this.scale, "Scale", min: 0.001f);

			// fields for fractal noise settings
			layout.Par(20);
			layout.Field(ref this.octaves, "Octaves", min: 0, max: 15);
			layout.Field(ref this.lacunarity, "Lacunarity");
			layout.Field(ref this.persistence, "Persistence");
			layout.Field(ref this.frequency, "Frequency");
		
			if(this.type==Type.Ridged)
			{				
				layout.Field(ref this.ridgedOffset, "Ridged Offset");
			
				/* check if on of the ridged noise specific UI element has changed to force 
				   reinitialize of the ridge specific spectral weights */
				if(layout.lastChange){
					this.dirtySpectralWeights = true;
				}	
			}	
		}

		/**
		@brief Generates the 2D Perlin Noise as Fractal Brownian Motion (fBm).

		Fractional Brownian Motion is the summation of successive octaves of noise, each with 
		higher frequency and lower amplitude.
	
		@see https://code.google.com/p/fractalterraingeneration/wiki/Fractional_Brownian_Motion

		@param x  The normalized x coordinate.
		@param z  The normalized z coordinate.
		@return float The multi-octave noise value.
		*/
		private float Fractal(float x, float z)
		{
            float total = 0f;
            float amplitude = 1f;
            float range = 0f;

            // store initial frequency value
            float detail = this.frequency;

            // generate the fractal noise
            for (int i = 0; i < this.octaves; i++)
            {
                total += noise.Generate(x, z, detail) * amplitude;
                range += amplitude;
                detail *= this.lacunarity;
                amplitude *= this.persistence;
            }

            // normalize value to range [-1,1]
            return total / range;
        }

		/**
		@brief Generates Billow noise as Fractal Brownian Motion (fBm) extension.

		This noise derivative has a puffy, cloud-like effect. 
	
		@see http://www.avanderw.co.za/beginnings-of-coherent-noise/
		@see http://accidentalnoise.sourceforge.net/

		@param x  The normalized x coordinate.
		@param y  The normalized z coordinate.
		@return float The multi-octave billow noise value.
		*/
		private float Billow(float x, float z)
		{
			float total = 0f;
			float amplitude = this.persistence;

			// store initial frequency value
			float detail = this.frequency;

			//generate the billow noise
			for (int i = 0; i < this.octaves; i++)
			{
				var signal = 2.0f * Mathf.Abs(this.noise.Generate(x, z, detail)) - 1.0f;
				total += signal * amplitude;
				detail *= this.lacunarity;
				amplitude *= this.persistence;
			}
			return total+0.5f;
		}
	
		/**
		@brief 2D Ridged noise as Fractal Brownian Motion (fBm) extension.
  
		Ridged multi fractal noise is generated in much of the same way as 
		the fractal noise (fBm) algorithm, except the output of each octave
		is modified by an absolute-value function to produces ridge-like 
		formations.
  
		@see http://www.texturingandmodeling.com/Musgrave.html
		@see http://www.texturingandmodeling.com/CODE/MUSGRAVE/CLOUD/fractal.c
  
		@note
		The following code is based on Ken Musgrave's explanations and sample
		source code in the book "Texturing and Modelling: A procedural approach"
  
		@see Texturing and Modeling: A Procedural Approach, ISBN 1-55860-848-6
	 
		@param x  The x coordinate.
		@param y  The y coordinate.
		@return float The multi-octave ridged noise value.
		*/
		private float Ridged(float x, float z)
		{		
			float weight = 1.0f;		
		
			// store initial frequency value
			float detail = this.frequency;

			/* Get the coherent-noise value of the first octave and determine 
			   the absolute value of the value to create the ridges.*/
			var signal = this.noise.Generate(x, z, detail);
			signal = this.ridgedOffset - Mathf.Abs(signal);

			// Square the signal to increase the sharpness of the ridges.
			signal *= signal;

			var total = signal;
			detail = this.lacunarity;
			for (int i = 1; i < this.octaves && weight > 0.001f; i++)
			{
				//weight depends on strength of previous signal
				weight = Mathf.Clamp(signal * this.lacunarity, 0.0f, 1.0f);

				//get the coherent-noise value of the actual octave
				signal = this.noise.Generate(x, z, detail);
				signal = this.ridgedOffset - Mathf.Abs(signal);
				signal *= signal;
		
				/* The weighting from the previous octave is applied to the signal.
				   Larger values have higher weights, producing sharp points along the
				   ridges. */
				signal *= weight;
						
				// Add the signal to the output value.
				total += (signal * this.spectralWeights[i]);
				detail *= this.lacunarity;
			}
            return ((total * 1.25f) - 1f) * 0.35f;
        }
		
		/* @brief Compute the spectral weight for each frequency used by ridged noise algorithm. */
		private void ComputeSpectralWeights()
		{
			if(this.dirtySpectralWeights==false || this.type != Type.Ridged){
				return;
			}
				
			float frequency = 1.0f;
			this.spectralWeights.Clear();
		
			for (int i = 0; i < this.maxOctaves; i++) {
				this.spectralWeights.Add((float)Mathf.Pow(frequency, -this.persistence));
				frequency *= this.lacunarity;
			}
			this.dirtySpectralWeights = false;
		}
	
		/*
		@brief Utility method to set the active noise provider based on given noise generator type.
		@remark The generator seed is based on the sum of the editor seed and global seed.
		@throws InvalidOperationException if noise generator type is not supported.
		@param baseType The noise provider type to set.
		@param seed The seed value to set for pseudo random value generation.
		*/
		private void initNoiseProvider(Base baseType, int seed)
		{			
			switch(baseType)
			{
				case Base.Perlin:
					this.noise = new PerlinNoiseProvider(seed);
					break;
				case Base.Simplex:
					this.noise = new SimplexNoiseProvider(seed);
					break;
				default:
					throw new InvalidOperationException("Unsupported noise provider type!");
			}	
		}	
	
			/*
		@brief Utility method to set the active noise delegate method based on given algorithm type.
		@throws InvalidOperationException if noise algorithm type is not supported.
		@param type The noise algorithm type to set.
		*/
		private void initNoiseMethod(Type type)
		{
			switch(type)
			{
				case Type.Fractal:
					this.generateNoise = new NoiseDelegate(Fractal);
					break;
				case Type.Billow:
					this.generateNoise = new NoiseDelegate(Billow);
					break;
				case Type.Ridged:
				{
					// also compute required spectral weights once on activation
					this.ComputeSpectralWeights(); 
					this.generateNoise = new NoiseDelegate(Ridged);			
					break;
				}
				default:
					throw new InvalidOperationException("Unsupported noise algorithm type!");
			}	
		}
	}
}