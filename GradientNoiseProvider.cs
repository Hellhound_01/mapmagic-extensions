﻿/**
  @class  GradientNoiseProvider
  @date   15/03/2016
  @author Hellhound

  @brief 
  Abstraction for a Gradient noise provider.
  
  Updated :
  Status  : FINAL
  Copyright Binary Revolution, Inc.  All rights reserved.
*/
using System.Linq;
using UnityEngine;
	
namespace MapMagicExternalGenerators
{
	public abstract class GradientNoiseProvider 
	{
		/* Permutation hash lookup table as defined by Ken Perlin.  This will be a randomly
		   arranged array of all numbers from 0-255 inclusive.*/
		protected int[] permutation = null;
				   
		// The seed
		protected int seed = 0;
		
		/** 
		@brief Create the Gradient noise provider instance based on given seed.
		@note This seed must be > 0 otherwise the actual time stamp of the System.Random 
			  class will be used as default value.       
		@param seed The seed used for random value creation. (Default == 0).
		*/
		public GradientNoiseProvider(int seed=0)
		{
			this.SetSeed(seed);
		}

		/**
		@brief Return the actual seed, which has been used to create the permutation lookup table.
		@return int The seed value.
		*/
		public int getSeed() {
			return this.seed;
		}

		/**
		@brief Set the seed and create a new permutation lookup table.
		@remark The seed must be > 0 otherwise the actual tick count is used as seed. 
		@param seed The seed used for random value creation. 
		*/
		public void SetSeed(int seed)
		{
			// Check if seed is given, otherwise use default time seed		
			if(seed>0){
				this.seed = seed;
			} else {
				this.seed = System.Environment.TickCount;
			}
			
			// create random value generator based on given seed
			var rand = new System.Random(this.seed);
			
			/* Create new lookup table Please take note that this array is doubled 
			   to avoid buffer overflows */
			this.permutation = new int[512];
			
			/* Generate a sequence of numbers in range [0,255], where
			   each number is one greater than the previous*/ 
			int[] p = Enumerable.Range(0, 256).ToArray();

			// Shuffle the array
			for (var i = 0; i < p.Length; i++)
			{
				var source = rand.Next(p.Length);
				var t = p[i];
				p[i] = p[source];
				p[source] = t;
			}

			/* Assign the values of the temporary array to the permutation array
			   and double it */
			for (int i = 0; i < 256; i++){
				this.permutation[i] = this.permutation[256+i] = p[i];
			}
		}

		/**
		@brief Generate a 1D coherent noise value.
		@param x  The x coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The coherent noise value in range [-1,1]. 
		*/
		public abstract float Generate(float x, float frequency);
		/**
		@brief Generate a 2D coherent noise value.
		@param x  The x coordinate.
		@param y  The y coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The coherent noise value in range [-1,1]. 
		*/
		public abstract float Generate(float x, float y, float frequency);
		
		/**
		@brief Generate a 3D Perlin noise value.
		@param x  The x coordinate.
		@param y  The y coordinate.
		@param z  The z coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The Perlin noise value in range [-1,1]. 
		*/
		public abstract float Generate(float x, float y, float z, float frequency);
	}
}